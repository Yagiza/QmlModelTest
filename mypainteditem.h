#ifndef MYPAINTEDITEM_H
#define MYPAINTEDITEM_H

#include <QQuickPaintedItem>

#include "mymodel.h"

class MyPaintedItem : public QQuickPaintedItem
{
	Q_OBJECT
	Q_PROPERTY(RosterIndex * index READ index WRITE setIndex NOTIFY indexChanged)

	public:
		MyPaintedItem(QQuickItem *parent = 0);
		void paint(QPainter *painter);

		RosterIndex *index() const;
		void setIndex(RosterIndex *_index);

	private:
		RosterIndex *m_index;

	signals:
		void indexChanged();
};

#endif // MYPAINTEDITEM_H
