#include "mypainteditem.h"

#include <QPainter>
#include <QBrush>

//! [0]
MyPaintedItem::MyPaintedItem(QQuickItem *parent)
	: QQuickPaintedItem(parent)
	, m_index(NULL)
{
	setImplicitHeight(40);
}
//! [0]

//! [1]
void MyPaintedItem::paint(QPainter *painter)
{
	painter->setBrush(Qt::NoBrush);
	painter->setPen(Qt::blue);
	painter->setRenderHint(QPainter::Antialiasing);

	painter->drawRoundedRect(0, 0, boundingRect().width(), boundingRect().height(), 5, 5);

	painter->setPen(QColor(Qt::yellow));
	painter->drawText(QRect(5,0,boundingRect().width()-10, 20), m_index->name());
	painter->setPen(QColor(Qt::gray));
	painter->drawText(QRect(5,20,boundingRect().width()-10, 20), m_index->status());
//	qDebug() << "name=" << m_index->name() << ";" << "status=" << m_index->status();
}

RosterIndex *MyPaintedItem::index() const
{
	return m_index;
}

void MyPaintedItem::setIndex(RosterIndex *_index)
{
	m_index = _index;
}
