import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import Example 1.0

Window {
    visible: true

    ColumnLayout {
             id: columnLayout
             anchors.fill: parent

         Component {
             id: contactDelegate
             MyPaintedItem {
                index: model.rosterIndex
                width: listView.width;
                MouseArea {
                    anchors.fill: parent
                    onClicked: { listView.currentIndex = model.index }
                }
             }
         }

         ListView {
             id: listView
             model: myModel
             anchors.fill: parent
             width: parent.width
             height: parent.height
             delegate: contactDelegate
             highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
             focus: true
         }
     }
}
