#ifndef MYMODEL_H
#define MYMODEL_H

#include <QAbstractItemModel>

class RosterIndex
{
public:
	RosterIndex(const QString &name=QString(), const QString &status=QString());

	QString name() const;
	QString status() const;

private:
	QString m_name;
	QString m_status;
};

class MyModel : public QAbstractListModel
{
	Q_OBJECT
public:
	enum IndexRoles {
		RosterIndexRole = Qt::UserRole + 1
	};

	MyModel(QObject *parent = 0);
//![1]

	void addAnimal(RosterIndex *animal);

	int rowCount(const QModelIndex & parent = QModelIndex()) const;

	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
	QHash<int, QByteArray> roleNames() const;
private:
	QList<RosterIndex*> indexes;
};

Q_DECLARE_METATYPE(RosterIndex*)

#endif // MYMODEL_H
