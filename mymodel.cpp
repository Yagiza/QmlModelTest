#include "mymodel.h"

RosterIndex::RosterIndex(const QString &name, const QString &status)
	: m_name(name), m_status(status)
{
}

QString RosterIndex::name() const
{
	return m_name;
}

QString RosterIndex::status() const
{
	return m_status;
}

MyModel::MyModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

void MyModel::addAnimal(RosterIndex *animal)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	indexes << animal;
	endInsertRows();
}

int MyModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return indexes.count();
}

QVariant MyModel::data(const QModelIndex & index, int role) const {
	if (index.row() < 0 || index.row() >= indexes.count())
		return QVariant();

//	const RosterIndex &animal = indexes[index.row()];
//	if (role == NameRole)
//		return animal.name();
//	else if (role == StatusRole)
//		return animal.status();
	if (role == RosterIndexRole)
		return QVariant::fromValue<RosterIndex*>(indexes[index.row()]);
	return QVariant();
}

QHash<int, QByteArray> MyModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[RosterIndexRole] = "rosterIndex";
//	roles[StatusRole] = "status";
	return roles;
}
