#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

#include "mymodel.h"
#include "mypainteditem.h"

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);
	MyModel model;

	qmlRegisterType<MyPaintedItem>("Example", 1, 0, "MyPaintedItem");

#if 1
	QQmlApplicationEngine engine;
	QQmlContext *ctxt = engine.rootContext();
	ctxt->setContextProperty("myModel", &model);
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
#else
	QQuickView view;
	view.setResizeMode(QQuickView::SizeRootObjectToView);
	view.setSource(QUrl("qrc:/main.qml"));
	QQmlContext *ctxt = view.rootContext();
	view.show();
#endif

	QList<RosterIndex *> indexes;
	indexes << new RosterIndex("Wolf", "Medium")
			<< new RosterIndex("Polar bear", "Large")
			<< new RosterIndex("Quoll", "Small");

	for (QList<RosterIndex *>::ConstIterator it = indexes.constBegin(); it!=indexes.constEnd(); ++it)
		model.addAnimal(*it);

	int rc = app.exec();

	while (!indexes.isEmpty())
		delete indexes.takeFirst();

	return rc;
}

